FROM openjdk:11-oracle
VOLUME /tmp
COPY . .
ENTRYPOINT ["java", "-jar","/app.jar", "--spring.profiles.active=docker"]
