package com.regystone.wedodecoapi.exception;


import com.regystone.wedodecoapi.enums.ResultEnum;

/**
 * Created By Regystone 05/08/2019.
 */
public class MyException extends RuntimeException {

    private Integer code;

    public MyException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());

        this.code = resultEnum.getCode();
    }

    public MyException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
