package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.Piece;

public interface PieceService {

    Piece findOne(Long pieceId);
    Piece save(Piece piece);
}
