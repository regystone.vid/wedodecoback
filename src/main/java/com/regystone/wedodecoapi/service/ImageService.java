package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.ProductInfo;
import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.User;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

    void store(MultipartFile file, User user);

    Resource loadFile(String filename);
    Resource loadProjectFiles(String filename, Long projectId) ;
    void deleteAll();
    void deletePhoto(String filename);
    void init();
    void initProject();
    void initProduct();
    void deleteProject(String filename, Long projectId);
    void deleteProduct(String filename, Long productId);

    void deleteOneProject(Long projectId);

    void storeProject(MultipartFile file, Project project);
    void storeProduct(MultipartFile file, ProductInfo productInfo);
    Resource loadProductFile(String filename, Long projectId);
}
