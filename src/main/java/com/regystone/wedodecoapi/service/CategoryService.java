package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.ProductCategory;

import java.util.List;

/**
 * Created By Regystone 05/08/2019.
 */
public interface CategoryService {

    List<ProductCategory> findAll();

    ProductCategory findByCategoryType(Integer categoryType);

    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

    ProductCategory save(ProductCategory productCategory);


}
