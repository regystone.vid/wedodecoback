package com.regystone.wedodecoapi.service.impl;

import com.regystone.wedodecoapi.entity.Piece;
import com.regystone.wedodecoapi.repository.PieceRepository;
import com.regystone.wedodecoapi.service.PieceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PieceServiceImpl implements PieceService {

    @Autowired
    PieceRepository pieceRepository;


    @Override
    public Piece findOne(Long pieceId) {

        Piece piece = pieceRepository.findByPieceId(pieceId);
        return piece;
    }

    @Override
    public Piece save(Piece piece) {
        return piece;
    }
}
