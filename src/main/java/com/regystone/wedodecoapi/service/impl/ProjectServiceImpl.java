package com.regystone.wedodecoapi.service.impl;

import com.regystone.wedodecoapi.entity.*;
import com.regystone.wedodecoapi.enums.ResultEnum;
import com.regystone.wedodecoapi.exception.MyException;
import com.regystone.wedodecoapi.form.ProjectForm;
import com.regystone.wedodecoapi.repository.AnimalRepository;
import com.regystone.wedodecoapi.repository.PieceRepository;
import com.regystone.wedodecoapi.repository.ProjectRepository;
import com.regystone.wedodecoapi.repository.UserRepository;
import com.regystone.wedodecoapi.service.EmailService;
import com.regystone.wedodecoapi.service.ProjectService;
import com.regystone.wedodecoapi.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ProjectServiceImpl implements ProjectService {


    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;



    @Autowired
    PieceRepository pieceRepository;

    @Autowired
    AnimalRepository animalRepository;
    private final Path rootLocation = Paths.get("project-dir");

    @Override
    @Transactional
    public Project createProject(ProjectForm projectForm, User user) {
        Project project = projectRepository.save(projectForm.getProject());
        Set<Piece> pieceList = new HashSet<>();
        projectForm.getPieces().forEach(piec -> {
            Piece piece = piec;
            piece.setProject(project);
            pieceList.add(piece);
            pieceRepository.save(piece);

        });

        Set<Animal> animalList = new HashSet<>();
        projectForm.getAnimals().forEach(ani -> {
            Animal animal = ani;
            animal.setProject(project);
            animalList.add(animal);
            animalRepository.save(animal);
        });

        project.setProjectStatus(0);
        project.setPopupStatus(0);
        LocalDateTime date = LocalDateTime.now();

        project.setCreateTime(date);
        project.setUser(user);
        project.setUserProjectId(user.getId());
        projectRepository.save(project);
        File theDir = new File(rootLocation+"/project_"+project.getProjectId());

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try{
                theDir.mkdir();
                result = true;
            }
            catch(SecurityException se){
                //handle it
            }
            if(result) {
                System.out.println("DIR created");
            }
        }



        return projectRepository.save(project);
    }


    @Override
    public Page<Project> findAll(Pageable pageable) {
        return projectRepository.findAllByOrderByProjectStatusAscCreateTimeDesc(pageable);
    }

    @Override
    public Page<Project> findByStatus(Integer status, Pageable pageable) {
        return projectRepository.findAllByProjectStatusOrderByCreateTimeDesc(status, pageable);
    }

    @Override
    public Page<Project> findByUser(User user, Pageable pageable) {
        return projectRepository.findAllByUserOrderByCreateTimeDesc(user, pageable);
    }



    @Override
    public Project findOne(Long projectId) {
        Project project = projectRepository.findByProjectId(projectId);
        if(project == null) {
            throw new MyException(ResultEnum.PROJECT_NOT_FOUND);
        }
        return project;
    }

    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    @Override
    @Transactional
    public Project update(Project project, User user) {
        Project oldProject = projectRepository.findByProjectId(project.getProjectId());
        oldProject.setProjectStatus(project.getProjectStatus());
        oldProject.setUpdateTime(LocalDateTime.now());
        oldProject.setUser(user);
        oldProject.setPopupStatus(project.getPopupStatus());
        oldProject.setProjectAmount(project.getProjectAmount());
        return projectRepository.save(oldProject);
    }

    @Override
    @Transactional
    public void delete(Long projectId) {
        Project project = findOne(projectId);
        if (project == null) throw new MyException(ResultEnum.PROJECT_NOT_FOUND);
        projectRepository.delete(project);
    }


}
