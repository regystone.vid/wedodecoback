package com.regystone.wedodecoapi.service.impl;

import com.regystone.wedodecoapi.entity.ProductInfo;
import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.User;
import com.regystone.wedodecoapi.repository.ProjectRepository;
import com.regystone.wedodecoapi.repository.UserRepository;
import com.regystone.wedodecoapi.service.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageServiceImpl implements ImageService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    ProjectRepository projectRepository;

    Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final Path rootLocation = Paths.get("user-dir");
    private final Path rootLocationProject = Paths.get("project-dir");
    private final Path rootLocationProduct = Paths.get("product-dir");

    public void store(MultipartFile file, User user) {
        try {
            user.setImageUrl(user.getId()+file.getOriginalFilename());
            userRepository.save(user);
            Files.copy(file.getInputStream(), this.rootLocation.resolve(user.getId()+file.getOriginalFilename()));
        } catch (Exception e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public void deletePhoto(String filename) {
        FileSystemUtils.deleteRecursively(rootLocation.resolve(filename).toFile());
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocationProject.toFile());
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    public void deleteProject(String filename, Long projectId) {
        final Path rootLocationOneProject = Paths.get("project-dir/project_"+projectId);
        FileSystemUtils.deleteRecursively(rootLocationOneProject.resolve(filename).toFile());
    }

    public void deleteProduct(String filename, Long projectId) {
        final Path rootLocationPathOneProduct = Paths.get("product-dir/product_"+projectId);
        FileSystemUtils.deleteRecursively(rootLocationPathOneProduct.resolve(filename).toFile());
    }

    public void deleteOneProject(Long projectId) {
        final Path rootLocationOneProject = Paths.get("project-dir/project_"+projectId);
        FileSystemUtils.deleteRecursively(rootLocationOneProject.toFile());
    }

    public void initProject() {
        try {
            Files.createDirectory(rootLocationProject);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    public void initProduct() {
        try {
            Files.createDirectory(rootLocationProduct);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    public void storeProject(MultipartFile file, Project project) {
        try {
            final Path rootLocationOneProject = Paths.get("project-dir/project_"+project.getProjectId());
            Files.copy(file.getInputStream(), rootLocationOneProject.resolve(project.getProjectId()+file.getOriginalFilename()));
        } catch (Exception e) {
            throw new RuntimeException("FAIL!" + e);
        }
    }

    public void storeProduct(MultipartFile file, ProductInfo productInfo) {
        try {
            final Path rootLocationOneProduct = Paths.get("product-dir/product_"+productInfo.getProductId());
            Files.copy(file.getInputStream(), rootLocationOneProduct.resolve(productInfo.getProductId()+file.getOriginalFilename()));
        } catch (Exception e) {
            throw new RuntimeException("FAIL!" + e);
        }
    }

    public Resource loadProductFile(String filename, Long projectId) {
        final Path rootLocationOneProduct = Paths.get("product-dir/product_"+projectId);
        try {
            Path file = rootLocationOneProduct.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public Resource loadProjectFiles(String filename, Long projectId) {
        final Path rootLocationOneProject = Paths.get("project-dir/project_"+projectId);
        try {
            Path file = rootLocationOneProject.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

}
