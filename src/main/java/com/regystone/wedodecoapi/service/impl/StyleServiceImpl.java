package com.regystone.wedodecoapi.service.impl;

import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.Style;
import com.regystone.wedodecoapi.enums.ResultEnum;
import com.regystone.wedodecoapi.exception.MyException;
import com.regystone.wedodecoapi.repository.StyleRepository;
import com.regystone.wedodecoapi.service.StyleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class StyleServiceImpl implements StyleService {


    @Autowired
    StyleRepository styleRepository;

    Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final Path rootLocation = Paths.get("upload-dir");

    public void store(MultipartFile file, Style style) {
        try {
            Files.copy(file.getInputStream(), this.rootLocation.resolve(style.getStyleId()+"_"+file.getOriginalFilename()));
        } catch (Exception e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public void deleteOne(String filename) {
        FileSystemUtils.deleteRecursively(rootLocation.resolve(filename).toFile());
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    @Override
    public Style getStyleByName(String styleName){
        return(styleRepository.findByStyleName(styleName));
    }



    @Override
    public Page<Style> findAll(Pageable pageable) {
        return styleRepository.findAll(pageable);
    }

    @Override
    public Style findOne(Long styleId) {
        Style style = styleRepository.findByStyleId(styleId);
        if(style == null) {
            throw new MyException(ResultEnum.PROJECT_NOT_FOUND);
        }
        return style;
    }


}
