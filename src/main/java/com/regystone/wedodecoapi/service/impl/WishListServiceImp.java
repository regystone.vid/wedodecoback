package com.regystone.wedodecoapi.service.impl;

import com.regystone.wedodecoapi.entity.OrderMain;
import com.regystone.wedodecoapi.entity.ProductInOrder;
import com.regystone.wedodecoapi.entity.User;
import com.regystone.wedodecoapi.entity.WishList;
import com.regystone.wedodecoapi.repository.OrderRepository;
import com.regystone.wedodecoapi.repository.ProductInOrderRepository;
import com.regystone.wedodecoapi.repository.UserRepository;
import com.regystone.wedodecoapi.repository.WishListRepository;
import com.regystone.wedodecoapi.service.ProductService;
import com.regystone.wedodecoapi.service.UserService;
import com.regystone.wedodecoapi.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

/**
 * Created By Regystone 05/08/2019.
 */
@Service
public class WishListServiceImp implements WishListService {
    @Autowired
    ProductService productService;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductInOrderRepository productInOrderRepository;
    @Autowired
    WishListRepository wishListRepository;
    @Autowired
    UserService userService;

    @Override
    public WishList getWishList(User user) {
        return user.getWishList();
    }

    @Override
    @Transactional
    public void mergeLocalWishList(Collection<ProductInOrder> productInOrders, User user) {
        WishList finalCart = user.getWishList();
        productInOrders.forEach(productInOrder -> {
            Set<ProductInOrder> set = finalCart.getProducts();
            Optional<ProductInOrder> old = set.stream().filter(e -> e.getProductId().equals(productInOrder.getProductId())).findFirst();
            ProductInOrder prod;
            if (old.isPresent()) {
                prod = old.get();
                prod.setCount(productInOrder.getCount() + prod.getCount());
            } else {
                prod = productInOrder;
                prod.setWishList(finalCart);
                finalCart.getProducts().add(prod);
            }
            productInOrderRepository.save(prod);
        });
        wishListRepository.save(finalCart);

    }

    @Override
    @Transactional
    public void delete(String itemId, User user) {
        var op = user.getWishList().getProducts().stream().filter(e -> itemId.equals(e.getProductId())).findFirst();
        op.ifPresent(productInOrder -> {
            productInOrder.setWishList(null);
            productInOrderRepository.deleteById(productInOrder.getId());
        });
    }



    @Override
    @Transactional
    public void checkout(User user) {
        // Creat an order
        OrderMain order = new OrderMain(user);
        orderRepository.save(order);
        // clear cart's foreign key & set order's foreign key& decrease stock
        user.getWishList().getProducts().forEach(productInOrder -> {
            productInOrder.setWishList(null);
            productInOrder.setOrderMain(order);
            productService.decreaseStock(productInOrder.getProductId(), productInOrder.getCount());
            productInOrderRepository.save(productInOrder);
        });
    }
}
