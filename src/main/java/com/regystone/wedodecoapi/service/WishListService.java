package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.ProductInOrder;
import com.regystone.wedodecoapi.entity.User;
import com.regystone.wedodecoapi.entity.WishList;

import java.util.Collection;

/**
 * Created By Regystone 05/08/2019.
 */
public interface WishListService {
    WishList getWishList(User user);

    void mergeLocalWishList(Collection<ProductInOrder> productInOrders, User user);

    void delete(String itemId, User user);

    void checkout(User user);
}
