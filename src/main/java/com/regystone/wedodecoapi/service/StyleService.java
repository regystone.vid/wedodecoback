package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.Style;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface StyleService {

    Page<Style> findAll(Pageable pageable);

    Style findOne(Long styleId);

    void store(MultipartFile file, Style style);

    Resource loadFile(String filename);
    public void deleteOne(String filename);

    void deleteAll();

    void init();

    Style getStyleByName(String styleName);

}
