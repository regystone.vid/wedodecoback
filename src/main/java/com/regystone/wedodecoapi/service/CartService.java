package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.Cart;
import com.regystone.wedodecoapi.entity.ProductInOrder;
import com.regystone.wedodecoapi.entity.User;

import java.util.Collection;

/**
 * Created By Regystone 05/08/2019.
 */
public interface CartService {
    Cart getCart(User user);

    void mergeLocalCart(Collection<ProductInOrder> productInOrders, User user);

    void delete(String itemId, User user);
    void deleteAll(User user);
    void checkout(User user);
}
