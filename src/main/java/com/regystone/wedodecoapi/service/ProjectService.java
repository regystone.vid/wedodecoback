package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.User;
import com.regystone.wedodecoapi.form.ProjectForm;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface ProjectService {

    Project createProject(ProjectForm projectForm, User user);
    Page<Project> findAll(Pageable pageable);
    Page<Project> findByStatus(Integer status, Pageable pageable);
    Project findOne(Long projectId);
    Page<Project> findByUser(User user, Pageable pageable);
    Resource loadFile(String filename);
    Project update(Project project, User user);
    void delete(Long projectId);
}
