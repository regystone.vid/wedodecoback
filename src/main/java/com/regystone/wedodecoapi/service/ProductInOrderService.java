package com.regystone.wedodecoapi.service;

import com.regystone.wedodecoapi.entity.ProductInOrder;
import com.regystone.wedodecoapi.entity.User;

/**
 * Created By Regystone 05/08/2019.
 */
public interface ProductInOrderService {
    void update(String itemId, Integer quantity, User user);
    ProductInOrder findOne(String itemId, User user);
}
