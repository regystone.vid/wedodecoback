package com.regystone.wedodecoapi.service;


import com.regystone.wedodecoapi.entity.User;

import java.util.Collection;

/**
 * Created By Regystone 05/08/2019.
 */
public interface UserService {
    User findOne(String email);

    Collection<User> findByRole(String role);

    User save(User user);

    User update(User user);


    void updatePassword(String updatedPassword, Long id);

}
