package com.regystone.wedodecoapi.enums;

/**
 * Created By Regystone 05/08/2019.
 */
public enum ProjectOrderStatusEnum implements CodeEnum {
    NEW(0, "New OrderProjectMain"),
    FINISHED(1, "Finished"),
    CANCELED(2, "Canceled")
    ;

    private  int code;
    private String msg;

    ProjectOrderStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
