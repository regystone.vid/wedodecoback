package com.regystone.wedodecoapi.enums;

/**
 * Created By Regystone 05/08/2019.
 */
public interface CodeEnum {
    Integer getCode();

    String getMessage();
}
