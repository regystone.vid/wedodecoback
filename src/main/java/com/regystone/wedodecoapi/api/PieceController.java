package com.regystone.wedodecoapi.api;


import com.regystone.wedodecoapi.entity.Piece;
import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.repository.PieceRepository;
import com.regystone.wedodecoapi.service.PieceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RestController
public class PieceController {


    @Autowired
    PieceService pieceService;

    @Autowired
    PieceRepository pieceRepository;

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/seller/piece/new")
    public ResponseEntity create(@Valid @RequestBody Piece piece,
                                 BindingResult bindingResult) {
        Piece pieceIdExists = pieceService.findOne(piece.getPieceId());
        if (pieceIdExists != null) {
            bindingResult
                    .rejectValue("pieceId", "error.piece",
                            "There is already a piece with the code provided");
        }
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult);
        }
        return ResponseEntity.ok(pieceService.save(piece));
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/piece/{projectId}")
    public List<Piece> showOne(@PathVariable("projectId") Long projectId) {

        List<Piece> pieces = pieceRepository.findAllByProjectProjectId(projectId);

        return pieces;
    }

}
