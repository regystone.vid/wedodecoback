package com.regystone.wedodecoapi.api;


import com.regystone.wedodecoapi.entity.Animal;
import com.regystone.wedodecoapi.entity.Piece;
import com.regystone.wedodecoapi.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RestController
public class AnimalController {

    @Autowired
    AnimalRepository animalRepository;
    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/animal/{projectId}")
    public List<Animal> showOne(@PathVariable("projectId") Long projectId) {

        List<Animal> animals = animalRepository.findAllByProjectProjectId(projectId);

        return animals;
    }
}
