package com.regystone.wedodecoapi.api;

import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.Style;
import com.regystone.wedodecoapi.repository.StyleRepository;
import com.regystone.wedodecoapi.service.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RestController
@RequestMapping("/style")
@Component
public class StyleController {


    @Autowired
    StyleService styleService;

    @Autowired
    StyleRepository styleRepository;


    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/addStyle")
    public ResponseEntity<String> create(@RequestParam("styleName") String styleName,  @RequestParam("files") MultipartFile[] files) {
        try {
            List<String> file_s = new ArrayList<String>();
            Style styleObject = styleService.getStyleByName(styleName);
            if(styleObject != null){
                file_s = styleObject.getImagesUrls();
                for(var i =0 ; i<files.length;i++){
                    styleService.store(files[i], styleObject);
                    file_s.add(styleObject.getStyleId().toString()+"_"+files[i].getOriginalFilename());
                }
                styleObject.setImagesUrls(file_s);
                styleObject.setImageUrl("https://wedodeco.com/style/files/"+file_s.get(0));
                styleRepository.save(styleObject);
                return ResponseEntity.status(HttpStatus.OK).body("style updated");
            } else {
                Style style = new Style();
                style.setStyleName(styleName);
                Style styleEntity = styleRepository.save(style);
                for(var i =0 ; i<files.length;i++){
                    styleService.store(files[i], style);
                    file_s.add(styleEntity.getStyleId().toString()+"_"+files[i].getOriginalFilename());
                }
                styleEntity.setImagesUrls(file_s);
                styleRepository.save(styleEntity);
                return ResponseEntity.status(HttpStatus.OK).body("Style successfully added");
            }

    } catch (Exception e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("fail");
    }
    }


    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/list")
    public Page<Style> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                 @RequestParam(value = "size", defaultValue = "3") Integer size) {
        PageRequest request = PageRequest.of(page - 1, size);
        return styleService.findAll(request);
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = styleService.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/getallfiles/{styleId}")
    public ResponseEntity<List<String>> getListFiles(@PathVariable("styleId") Long styleId) {
        Style style = styleService.findOne(styleId);
        List<String> fileNames = style.getImagesUrls()
                .stream().map(fileName -> MvcUriComponentsBuilder
                        .fromMethodName(StyleController.class, "getFile", fileName).build().toString())
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(fileNames);
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/{styleId}")
    public Style showOne(@PathVariable("styleId") Long styleId) {
        Style style = styleService.findOne(styleId);
        return style;
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/styleName/{styleName}")
    public Style getStyleByName(@PathVariable("styleName") String styleName) {
        Style style = styleService.getStyleByName(styleName);
        if(style != null){
            return style;
        } else {
            return null;
        }
    }
}