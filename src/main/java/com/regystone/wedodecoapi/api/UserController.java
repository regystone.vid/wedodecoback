 package com.regystone.wedodecoapi.api;

 import com.regystone.wedodecoapi.entity.*;
 import com.regystone.wedodecoapi.form.MailForm;
 import com.regystone.wedodecoapi.form.PasswordForm;
 import com.regystone.wedodecoapi.repository.ConfirmationTokenRepository;
 import com.regystone.wedodecoapi.repository.PasswordResetTokenRepository;
 import com.regystone.wedodecoapi.repository.UserRepository;
 import com.regystone.wedodecoapi.security.JWT.JwtProvider;
 import com.regystone.wedodecoapi.service.EmailSenderService;
 import com.regystone.wedodecoapi.service.EmailService;
 import com.regystone.wedodecoapi.service.ImageService;
 import com.regystone.wedodecoapi.service.UserService;
 import com.regystone.wedodecoapi.vo.request.LoginForm;
 import com.regystone.wedodecoapi.vo.response.JwtResponse;
 import org.jetbrains.annotations.NotNull;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.core.io.InputStreamResource;
 import org.springframework.core.io.Resource;
 import org.springframework.data.domain.Page;
 import org.springframework.data.domain.PageRequest;
 import org.springframework.http.HttpStatus;
 import org.springframework.http.MediaType;
 import org.springframework.http.ResponseEntity;
 import org.springframework.mail.SimpleMailMessage;
 import org.springframework.security.authentication.AuthenticationManager;
 import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
 import org.springframework.security.core.Authentication;
 import org.springframework.security.core.AuthenticationException;
 import org.springframework.security.core.context.SecurityContextHolder;
 import org.springframework.security.core.userdetails.UserDetails;
 import org.springframework.web.bind.annotation.*;
 import org.springframework.web.multipart.MultipartFile;

 import javax.servlet.http.HttpServletRequest;
 import javax.transaction.Transactional;
 import java.io.IOException;
 import java.security.Principal;
 import java.util.*;

 /**
 * Created By Regystone 05/08/2019.
 */
 @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
 @RestController
public class UserController{

    @Autowired
    private UserRepository userRepository;

     @Autowired
     private PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private EmailSenderService emailSenderService;

     @Autowired
     private EmailService emailService;


    @Autowired
    UserService userService;

     @Autowired
     ImageService imageService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthenticationManager authenticationManager;

     List<String> files = new ArrayList<String>();

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody LoginForm loginForm) {
        // throws Exception if authentication failed

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginForm.getUsername(), loginForm.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtProvider.generate(authentication);
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            User user = userService.findOne(userDetails.getUsername());
            return ResponseEntity.ok(new JwtResponse(jwt, user.getEmail(), user.getFirstName() +" "+ user.getLastName(), user.getRole()));
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/loginSocial")
    public ResponseEntity<JwtResponse> loginSocial(@RequestBody User userS) {
        User user = userService.findOne(userS.getEmail());
        try {
            if (user != null) {
                return getJwtResponseResponseEntity(user);
            } else {
                try {
                    userS.setPassword(userS.getProviderId());
                    userService.save(userS);
                    User useR = userService.findOne(userS.getEmail());
                    return getJwtResponseResponseEntity(useR);
                } catch (AuthenticationException e) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
                }
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @NotNull
    private ResponseEntity<JwtResponse> getJwtResponseResponseEntity(User user) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getProviderId()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generate(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User userR = userService.findOne(userDetails.getUsername());
        return ResponseEntity.ok(new JwtResponse(jwt, userR.getEmail(), userR.getUserName(), userR.getRole()));
    }


     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/getEmail/{email}")
    public ResponseEntity<User> getEmail(@PathVariable("email") String email) {
        User existingUser = userRepository.findByEmail(email);
        if (existingUser != null) {
            return ResponseEntity.ok(existingUser);
        } else {
            return ResponseEntity.ok(null);
        }
    }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @GetMapping("/getId/{id}")
     public ResponseEntity<Optional <User>> getId(@PathVariable("id") Long id) {
         Optional <User> existingUser = userRepository.findById(id);
         if (existingUser != null) {
             return ResponseEntity.ok(existingUser);
         } else {
             return ResponseEntity.ok(null);
         }
     }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @PostMapping("/register")
    public ResponseEntity<User> save(@RequestBody User user) {
        User existingUser = userRepository.findByEmail(user.getEmail());
        if(existingUser != null){
            return ResponseEntity.badRequest().build();
        }
        else
        {
        try {

                userService.save(user);

                ConfirmationToken confirmationToken = new ConfirmationToken(user);

                confirmationTokenRepository.save(confirmationToken);
            Mail mail = new Mail();
            mail.setFrom("no-reply@wedodeco.com");
            mail.setTo(user.getEmail());
            mail.setSubject("Complete Registration!");

            Map<String, Object> model = new HashMap<>();
            model.put("token", confirmationToken);
            model.put("user", user);
            model.put("signature", "https://wedodeco.com");
            //String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            String url = "https://wedodeco.com/#";
            model.put("resetUrl", url + "/mail-verified?token="+confirmationToken.getConfirmationToken());
            mail.setModel(model);
            emailService.register(mail);

            System.out.println( "user " + user);
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            System.out.println( "error " + e);
            return ResponseEntity.badRequest().build();
        }
        }
    }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<User>  confirmUserAccount(@RequestParam("token")String confirmationToken)
    {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if(token != null)
        {
            User user = userRepository.findByEmail(token.getUser().getEmail());
            user.setActive(true);
            userRepository.save(user);
            return ResponseEntity.ok(user);
        }
        else
        {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public ConfirmationTokenRepository getConfirmationTokenRepository() {
        return confirmationTokenRepository;
    }

    public void setConfirmationTokenRepository(ConfirmationTokenRepository confirmationTokenRepository) {
        this.confirmationTokenRepository = confirmationTokenRepository;
    }

    public EmailSenderService getEmailSenderService() {
        return emailSenderService;
    }

    public void setEmailSenderService(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @PutMapping("/profile")
    public ResponseEntity<User> update(@RequestBody User user, Principal principal) {

        try {
            if (!principal.getName().equals(user.getEmail())) throw new IllegalArgumentException();
            return ResponseEntity.ok(userService.update(user));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @GetMapping("/profile/{email}")
    public ResponseEntity<User> getProfile(@PathVariable("email") String email, Principal principal) {
        if (principal.getName().equals(email)) {
            return ResponseEntity.ok(userService.findOne(email));
        } else {
            return ResponseEntity.badRequest().build();
        }

    }


    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/user/updatePhoto/{userEmail}")
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable("userEmail") String email) {
        String message = "";
        User user = userRepository.findByEmail(email);
        try {
                if(user.getImageUrl() != null){
                    imageService.deletePhoto(user.getImageUrl());
                }
                imageService.store(file, user);
                files.add(file.getOriginalFilename());
                message = "You successfully uploaded " + "!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
                message = "FAIL to upload "  + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }


     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @GetMapping("/user/files/{filename:.+}")
     @ResponseBody
     public ResponseEntity<Resource> getFile(@PathVariable String filename) throws IOException {
         System.out.println(  " file "+ filename);
         Resource file = imageService.loadFile(filename);
         if(file.getFilename().substring(file.getFilename().length() - 4, file.getFilename().length()).equals("jpg") || file.getFilename().substring(file.getFilename().length() - 5, file.getFilename().length()).equals("jpeg") ){
             return ResponseEntity.ok()
                     .contentType(MediaType.IMAGE_JPEG)
                     .body(new InputStreamResource(file.getInputStream()));
         } else {
             return ResponseEntity.ok()
                     .contentType(MediaType.IMAGE_PNG)
                     .body(new InputStreamResource(file.getInputStream()));
         }
     }


     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @PostMapping("/user/forgotPassword/{email}")
     public  ResponseEntity<String>  processForgotPasswordForm( @PathVariable String email,
                                             HttpServletRequest request) {
         User user = userService.findOne(email);
         if (user == null){
             return ResponseEntity.status(HttpStatus.OK).body( "\"We could not find an account for that e-mail address.\"");
         }

         PasswordResetToken token = new PasswordResetToken();
         token.setToken(UUID.randomUUID().toString());
         token.setUser(user);
         token.setExpiryDate(12);
         passwordResetTokenRepository.save(token);
         Mail mail = new Mail();
         mail.setFrom("no-reply@wedodeco.com");
         mail.setTo(user.getEmail());
         mail.setSubject("Password reset request");
         Map<String, Object> model = new HashMap<>();
         model.put("token", token);
         model.put("user", user);
         model.put("signature", "https://wedodeco.com");
         //String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
         String url = "https://wedodeco.com/#";
         model.put("resetUrl", url + "/reset-password?token=" + token.getToken());
         mail.setModel(model);
         emailService.resetPassword(mail);
         return ResponseEntity.status(HttpStatus.OK).body( "success");
     }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @GetMapping("/user/token/{token}")
     public  ResponseEntity<String>  displayResetPasswordPage(@PathVariable (required = false) String token) {
         PasswordResetToken resetToken = passwordResetTokenRepository.findByToken(token);
         if (resetToken == null){
             return ResponseEntity.status(HttpStatus.OK).body( "T \"Could not find password reset token.\"");
         } else if (resetToken.isExpired()){
             passwordResetTokenRepository.delete(resetToken);
             return ResponseEntity.status(HttpStatus.OK).body( "Token has expired, please request a new password reset.");
         } else {
             return ResponseEntity.status(HttpStatus.OK).body(resetToken.getToken());
         }
     }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @PostMapping("/user/updatePassword")
     @Transactional
     public ResponseEntity<String>  handlePasswordReset(@RequestBody PasswordForm passwordForm) {
         try {
             PasswordResetToken token = passwordResetTokenRepository.findByToken(passwordForm.getToken());
             if (token != null){
                 User user = token.getUser();
                 userService.updatePassword(passwordForm.getPassword(), user.getId());
                 passwordResetTokenRepository.delete(token);
                 return ResponseEntity.status(HttpStatus.OK).body("Password updated successfully");
             } else {
                 return ResponseEntity.status(HttpStatus.OK).body("token Doesn't exist");
             }
         }
         catch (Exception e) {
             return ResponseEntity.badRequest().body(e.toString());
         }
     }


     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @PostMapping("/user/passwordChange/{password}")
     @Transactional
     public ResponseEntity<String>  passwordChange(@PathVariable (required = false) String password,  Principal principal) {
         User user = userRepository.findByEmail(principal.getName());
         userService.updatePassword(password, user.getId());
         return ResponseEntity.status(HttpStatus.OK).body("Password updated successfully");
     }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @GetMapping("/allUsers")
     public Page<User> findProjectList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                          @RequestParam(value = "size", defaultValue = "3") Integer size, Principal principal) {
         PageRequest request = PageRequest.of(page - 1, size);
         return userRepository.findAllByRole("ROLE_CUSTOMER", request);
     }

     @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
     @PostMapping("/contact")
     public ResponseEntity<String> contactUs(@RequestBody MailForm email) {
             try {
                 Mail mail = new Mail();
                 mail.setFrom(email.getMail());
                 mail.setTo("contact@wedodeco.com");
                 mail.setSubject(email.getSubject());
                 Map<String, Object> model = new HashMap<>();
                 model.put("description", email.getDescription());
                 model.put("signature", "https://wedodeco.com");
                 mail.setModel(model);
                 emailService.contactUs(mail);
                 return ResponseEntity.status(HttpStatus.OK).body("mail successfully recieved");
             } catch (Exception e) {
                 return ResponseEntity.badRequest().build();
             }
     }

 }
