package com.regystone.wedodecoapi.api;


import com.regystone.wedodecoapi.entity.*;
import com.regystone.wedodecoapi.form.ProjectForm;
import com.regystone.wedodecoapi.repository.ProjectRepository;
import com.regystone.wedodecoapi.repository.UserRepository;
import com.regystone.wedodecoapi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.*;

@CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RestController
public class ProjectConroller {


    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;
    @Autowired
    PieceService pieceService;

    @Autowired
    ProjectService projectService;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    ImageService imageService;

    @Autowired
    private EmailService emailService;

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/project/new")
    public ResponseEntity create(@Valid @RequestBody ProjectForm projectForm, Principal principal) {
        System.out.println("Mohamed "+projectForm);
        User user = userService.findOne(principal.getName());
        return ResponseEntity.ok(projectService.createProject(projectForm, user));

    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PutMapping("/project/{id}/edit")
    public ResponseEntity confirmPaymentProject(@PathVariable("id") Long projectId,
                               @RequestBody ProjectForm project,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult);
        }
        if (!projectId.equals(project.getProject().getProjectId())) {
            return ResponseEntity.badRequest().body("Id Not Matched");
        }
        Mail mail = new Mail();
        mail.setFrom("no-reply@wedodeco.com");
        mail.setTo(project.getUser().getEmail());
        mail.setSubject("Payment Success!");

        Map<String, Object> model = new HashMap<>();
        model.put("user", project.getUser());
        model.put("project", project.getProject());
        model.put("signature", "https://wedodeco.com");
        //String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        String url = "https://wedodeco.com/#";
        model.put("resetUrl", url + "/login");
        mail.setModel(model);
        emailService.createProject(mail);
        return ResponseEntity.ok(projectService.update(project.getProject(), project.getUser()));
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/project/{projectId}")
    public Project showOne(@PathVariable("projectId") Long projectId) {
        Project project = projectService.findOne(projectId);
        return project;
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @DeleteMapping("/project/{projectId}/delete")
    public ResponseEntity deleteOne(@PathVariable("projectId") Long projectId) {
        imageService.deleteOneProject(projectId);
        projectService.delete(projectId);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/project/list")
    public Page<Project> findProjectList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                     @RequestParam(value = "size", defaultValue = "3") Integer size, Principal principal) {
        User user = userService.findOne(principal.getName());
        PageRequest request = PageRequest.of(page - 1, size);
        return projectService.findByUser(user ,request);
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/project/listAll")
    public List<Project> findProjectsList(Principal principal) {
        User user = userService.findOne(principal.getName());
        return projectRepository.findAllByUserIdOrderByCreateTimeDesc(user.getId());
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/project")
    public Page<Project> findAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                 @RequestParam(value = "size", defaultValue = "3") Integer size) {
        PageRequest request = PageRequest.of(page - 1, size);
        return projectService.findAll(request);
    }




    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/project/{projectId}/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename, @PathVariable Long projectId) throws IOException {
        Resource file = imageService.loadProjectFiles(filename, projectId);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename="+filename);
headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            return ResponseEntity.ok().headers(headers)
                    .body(new InputStreamResource(file.getInputStream()));

    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/seller/project/{id}/threedfile")
    public ResponseEntity<String> editPlanThreeD(@PathVariable("id") Long projectId,
                               @RequestParam("file") MultipartFile file) {
        String message = "";
        Project project = projectService.findOne(projectId);
        try {
            if (!projectId.equals(project.getProjectId())) {
                return ResponseEntity.badRequest().body("Id Not Matched");
            }
            if(project.getPlan3D() != null){
                int len = ("https://wedodeco.com/project/"+project.getProjectId()+"/files/").length();
                String result = project.getPlan3D().substring(len, project.getPlan3D().length());
                imageService.deleteProject(result, projectId);
            }
            imageService.storeProject(file, project);
            message = "You successfully uploaded " + "!";
            project.setPlan3D("https://wedodeco.com/project/"+project.getProjectId()+"/files/"+project.getProjectId()+file.getOriginalFilename());
            projectRepository.save(project);
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "FAIL to upload "  + e + "! ";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }


    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PutMapping("/seller/project/{id}/edit")
    public ResponseEntity edit(@PathVariable("id") Long projectId,
                               @RequestBody ProjectForm project,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult);
        }
        if (!projectId.equals(project.getProject().getProjectId())) {
            return ResponseEntity.badRequest().body("Id Not Matched");
        }
        return ResponseEntity.ok(projectService.update(project.getProject(), project.getUser()));
    }



    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/project/addProjectImages")
    public ResponseEntity<String> create(@RequestParam("projectId") Long projectId,  @RequestParam("files") MultipartFile[] files) {
        try {
            List<String> file_s = new ArrayList<String>();
            Project projectObject = projectRepository.findByProjectId(projectId);
                file_s = projectObject.getImagesUrls();
                for(var i =0 ; i<files.length;i++){
                    imageService.storeProject(files[i], projectObject);
                    file_s.add("https://wedodeco.com/project/"+projectObject.getProjectId()+"/files/"+projectObject.getProjectId()+files[i].getOriginalFilename());
                }
                projectObject.setImagesUrls(file_s);
                projectRepository.save(projectObject);
                return ResponseEntity.status(HttpStatus.OK).body("project updated");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("fail");
        }
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/project/addProjectImagesBack")
    public ResponseEntity<String> createImages(@RequestParam("projectId") Long projectId,  @RequestParam("files") MultipartFile[] files) {
        try {
            List<String> file_s = new ArrayList<String>();
            Project projectObject = projectRepository.findByProjectId(projectId);
            file_s = projectObject.getImagesBeforeAfter();
            for(var i =0 ; i<files.length;i++){
                imageService.storeProject(files[i], projectObject);
                file_s.add("https://wedodeco.com/project/"+projectObject.getProjectId()+"/files/"+projectObject.getProjectId()+files[i].getOriginalFilename());
            }
            projectObject.setImagesBeforeAfter(file_s);
            projectRepository.save(projectObject);
            return ResponseEntity.status(HttpStatus.OK).body("project updated");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("fail");
        }
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/project/addProjectImagesDeuxD")
    public ResponseEntity<String> createImagesProject(@RequestParam("projectId") Long projectId,  @RequestParam("files") MultipartFile[] files) {
        try {
            List<String> file_s = new ArrayList<String>();
            Project projectObject = projectRepository.findByProjectId(projectId);
            file_s = projectObject.getImagesDeuxD();
            for(var i =0 ; i<files.length;i++){
                imageService.storeProject(files[i], projectObject);
                file_s.add("https://wedodeco.com/project/"+projectObject.getProjectId()+"/files/"+projectObject.getProjectId()+files[i].getOriginalFilename());
            }
            projectObject.setImagesDeuxD(file_s);
            projectRepository.save(projectObject);
            return ResponseEntity.status(HttpStatus.OK).body("project updated");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("fail");
        }
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @DeleteMapping("/project/{projectId}/deletedeuxdfile/{planName}")
    public ResponseEntity<String> deletePlan(@PathVariable("planName") String planName, @PathVariable("projectId") Long projectId) {
        try {
            imageService.deleteProject(planName, projectId);
            return ResponseEntity.status(HttpStatus.OK).body("project plan 2d deleted");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("fail");
        }
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @PostMapping("/project/deuxdfile")
    public ResponseEntity<String> addProjectPlan2D(@RequestParam("projectId") String projectId,
                                                 @RequestParam("file") MultipartFile file) {
        String message = "";
        Project project = projectService.findOne(Long.parseLong(projectId));
        try {
            if (Long.parseLong(projectId) != project.getProjectId()) {
                return ResponseEntity.badRequest().body("Id Not Matched");
            }
            if(project.getPlan2D() != null){
                imageService.deleteProject(project.getPlan2D(), Long.parseLong(projectId));
            }
            imageService.storeProject(file, project);
            project.setPlan2D("https://wedodeco.com/project/"+project.getProjectId()+"/files/"+project.getProjectId()+file.getOriginalFilename());
            projectRepository.save(project);
            message = "You successfully uploaded " + "!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "FAIL to upload "  + e + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    @GetMapping("/project/listByUser/{email}")
    public Page<Project> findProjectListByUser(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value = "size", defaultValue = "3") Integer size, @PathVariable("email")  String email) {
        User user = userRepository.findByEmail(email);
        PageRequest request = PageRequest.of(page - 1, size);
        return projectService.findByUser(user ,request);
    }
}
