package com.regystone.wedodecoapi.api;

import com.regystone.wedodecoapi.entity.ProductInOrder;
import com.regystone.wedodecoapi.entity.User;
import com.regystone.wedodecoapi.entity.WishList;
import com.regystone.wedodecoapi.form.ItemForm;
import com.regystone.wedodecoapi.repository.ProductInOrderRepository;
import com.regystone.wedodecoapi.service.ProductInOrderService;
import com.regystone.wedodecoapi.service.ProductService;
import com.regystone.wedodecoapi.service.UserService;
import com.regystone.wedodecoapi.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
/*
 * Created By Regystone 05/08/2019.
 */
@CrossOrigin(allowCredentials = "true", origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RestController
@RequestMapping("/wishList")
public class WishListController {
    @Autowired
    WishListService wishListService;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;
    @Autowired
    ProductInOrderService productInOrderService;
    @Autowired
    ProductInOrderRepository productInOrderRepository;


    @PostMapping("")
    public ResponseEntity<WishList> mergeCart(@RequestBody Collection<ProductInOrder> productInOrders, Principal principal) {
        User user = userService.findOne(principal.getName());
        try {
            wishListService.mergeLocalWishList(productInOrders, user);
        } catch (Exception e) {
            ResponseEntity.badRequest().body("Merge Cart Failed");
        }
        return ResponseEntity.ok(wishListService.getWishList(user));
    }

    @GetMapping("")
    public WishList getWishList(Principal principal) {
        User user = userService.findOne(principal.getName());
        return wishListService.getWishList(user);
    }


    @PostMapping("/add")
    public boolean addToWishList(@RequestBody ItemForm form, Principal principal) {
        var productInfo = productService.findOne(form.getProductId());
        try {
            mergeCart(Collections.singleton(new ProductInOrder(productInfo, form.getQuantity())), principal);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @PutMapping("/{itemId}")
    public ProductInOrder modifyItem(@PathVariable("itemId") String itemId, @RequestBody Integer quantity, Principal principal) {
        User user = userService.findOne(principal.getName());
        productInOrderService.update(itemId, quantity, user);
        return productInOrderService.findOne(itemId, user);
    }

    @DeleteMapping("/{itemId}")
    public void deleteItem(@PathVariable("itemId") String itemId, Principal principal) {
        User user = userService.findOne(principal.getName());
        wishListService.delete(itemId, user);
        // flush memory into DB
    }


    @PostMapping("/checkout")
    public ResponseEntity checkout(Principal principal) {
        User user = userService.findOne(principal.getName());// Email as username
        wishListService.checkout(user);
        return ResponseEntity.ok(null);
    }


}
