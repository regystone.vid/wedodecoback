package com.regystone.wedodecoapi;

import com.regystone.wedodecoapi.service.ImageService;
import com.regystone.wedodecoapi.service.ProjectService;
import com.regystone.wedodecoapi.service.StyleService;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.io.PrintStream;

@Controller
@SpringBootApplication(scanBasePackages={"com.regystone.wedodecoapi"})
public class ShopApiApplication extends SpringBootServletInitializer implements CommandLineRunner  {

    @Resource
    StyleService styleService;

    @Resource
    ImageService imageService;

    ProjectService projectService;



    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ShopApiApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(ShopApiApplication.class);
        springApplication.setBannerMode(Banner.Mode.CONSOLE);
        springApplication.run(args);
    }


    @Override
    public void run(String... arg) throws Exception {
        styleService.deleteAll();
        imageService.deleteAll();
        styleService.init();
        imageService.init();
        imageService.initProject();
        imageService.initProduct();
    }

}

