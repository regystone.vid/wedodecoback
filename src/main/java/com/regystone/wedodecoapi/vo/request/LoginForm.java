package com.regystone.wedodecoapi.vo.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created By Regystone 05/08/2019.
 */
@Data
public class LoginForm {


    @NotBlank
    private String username;
    @NotBlank
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
