package com.regystone.wedodecoapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created By Regystone 05/08/2019.
 */
@Data
@Entity
@NoArgsConstructor
public class WishList implements Serializable {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long wishListId;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JsonIgnore
//    @JoinColumn(name = "email", referencedColumnName = "email")
    private User user;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true,
            mappedBy = "wishList")
    private Set<ProductInOrder> products = new HashSet<>();

    public long getCartId() {
        return wishListId;
    }

    public void setCartId(long wishListId) {
        this.wishListId = wishListId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<ProductInOrder> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductInOrder> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "WishList{" +
                "wishListId=" + wishListId +
                ", products=" + products +
                '}';
    }

    public WishList(User user) {
        this.user  = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WishList wishList = (WishList) o;
        return wishListId == wishList.wishListId &&
                Objects.equals(user, wishList.user) &&
                Objects.equals(products, wishList.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wishListId, user, products);
    }
}
