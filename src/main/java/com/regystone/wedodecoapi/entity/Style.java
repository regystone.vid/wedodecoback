package com.regystone.wedodecoapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@DynamicUpdate
public class Style  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long styleId;

    private String styleName;

    private String imageUrl;

    //@Column
    @ElementCollection(targetClass=String.class)
    private List<String> imagesUrls;


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Project project;
}
