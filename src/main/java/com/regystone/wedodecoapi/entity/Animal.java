package com.regystone.wedodecoapi.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Getter
@Setter
@DynamicUpdate
public class Animal {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long animalId;

    private String animalName;

    private String imageUrl;

    @Min(0)
    private Integer count;

    private String info;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
//    @JoinColumn(name = "cart_id")
    @JsonIgnore
    private Project project;
}
