package com.regystone.wedodecoapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created By Regystone 05/08/2019.
 */
@Entity
@Getter
@Setter
@DynamicUpdate
public class Project implements Serializable {
    private static final long serialVersionUID = 4887904943282174032L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long projectId;


    private Long userProjectId;
    /** 0: In progress 1: done */
    private Integer projectStatus;

    private String projectName;

    private String plan3D;

    private String plan2D;

    private Integer plan2DStatus;

    private BigDecimal projectAmount;

    private String projectType;

    /** 0: In progress 1: doneBy User 2: Done By Employee */
    private Integer popupStatus;
    //@Column
    @ElementCollection(targetClass=String.class)
    private List<String> imagesUrls;

    @ElementCollection(targetClass=String.class)
    private List<String> imagesBeforeAfter;

    @ElementCollection(targetClass=String.class)
    private List<String> imagesDeuxD;

    @CreationTimestamp
    private LocalDateTime createTime;
    @UpdateTimestamp
    private LocalDateTime updateTime;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    //@JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore  // fix bi-direction toString() recursion problem
    private Set<Piece> pieces = new HashSet<>();

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore  // fix bi-direction toString() recursion problem
    private Set<Animal> animals = new HashSet<>();


}
