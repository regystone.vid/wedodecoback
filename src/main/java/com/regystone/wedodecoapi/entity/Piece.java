package com.regystone.wedodecoapi.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@DynamicUpdate
public class Piece {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pieceId;

    private String pieceName;

    private String pieceType;

    private String imagePiece;

    private String sexe;

    private String age;

    private String bed;

    private String occupant;

    private String sexeDeux;

    private String ageDeux;

    @Min(0)
    private Integer count;

    @Column
    @ElementCollection(targetClass=String.class)
    private List<String> imagesPieces;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "piece")
    private Set<ProductInfo> productInfos = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "cart_id")
    @JsonIgnore
    private Project project;

    //@Column
    @ElementCollection(targetClass=String.class)
    private List<String> styles;

}
