package com.regystone.wedodecoapi.form;

import lombok.Data;

@Data
public class MailForm {
    private String mail;
    private String subject;
    private String description;
}
