package com.regystone.wedodecoapi.form;


import com.regystone.wedodecoapi.entity.Animal;
import com.regystone.wedodecoapi.entity.Piece;
import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.User;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.Set;

@Data
public class ProjectForm {

    private Project project;

    private User user;

    private Set<Piece> pieces = new HashSet<>();

    private Set<Animal> animals = new HashSet<>();
}
