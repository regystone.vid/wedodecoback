package com.regystone.wedodecoapi.repository;

import com.regystone.wedodecoapi.entity.Piece;
import com.regystone.wedodecoapi.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PieceRepository  extends JpaRepository<Piece, Long> {

    Piece findByPieceId(Long id);
    List<Piece> findAllByProjectProjectId(Long projectId);
    void deleteAllByProjectProjectId(Long projectId);

}
