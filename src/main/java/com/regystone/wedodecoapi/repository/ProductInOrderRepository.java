package com.regystone.wedodecoapi.repository;

import com.regystone.wedodecoapi.entity.ProductInOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created By Regystone 05/08/2019.
 */
@Repository
public interface ProductInOrderRepository extends JpaRepository<ProductInOrder, Long> {

}
