package com.regystone.wedodecoapi.repository;

import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository  extends JpaRepository<Project, Long> {

    Page<Project> findAllByOrderByProjectStatusAscCreateTimeDesc(Pageable pageable);

    Page<Project> findAllByProjectStatusOrderByCreateTimeDesc(Integer projectStatus, Pageable pageable);

    Project findByProjectId(Long projectId);

    Page<Project> findAllByUserOrderByCreateTimeDesc(User user, Pageable pageable);
    List<Project> findAllByUserIdOrderByCreateTimeDesc(Long userId);
    void deleteByProjectId(Long projectId);

}
