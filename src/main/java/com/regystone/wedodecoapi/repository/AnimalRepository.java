package com.regystone.wedodecoapi.repository;

import com.regystone.wedodecoapi.entity.Animal;
import com.regystone.wedodecoapi.entity.Piece;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalRepository  extends JpaRepository<Animal, Long> {

    List<Animal> findAllByProjectProjectId(Long projectId);

}
