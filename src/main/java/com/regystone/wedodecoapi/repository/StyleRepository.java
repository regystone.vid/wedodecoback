package com.regystone.wedodecoapi.repository;

import com.regystone.wedodecoapi.entity.Project;
import com.regystone.wedodecoapi.entity.Style;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StyleRepository  extends JpaRepository<Style, Long> {
    Style findByStyleName(String styleName);
    Style findByStyleId(Long styleId);
}
